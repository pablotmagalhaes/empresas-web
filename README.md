<h1 align="center">
    <img alt="Ioasys Logo" title="#ioasys" width="285px" src=".github/logoioasys.png">
</h1>

# Índice

- [Sobre](#sobre)
- [Tecnologias Utilizadas](#tecnologias-utilizadas)
- [Como Usar](#como-usar)

<a id="sobre"></a>
## :bookmark: Sobre 

Aplicação desenvolvida como teste de avaliação submetida empresa pela ioasys.

## :heavy_check_mark: Resultado

<h4 align="center">
    <img alt="Tela Login" title="#login" width="800px" src=".github/tela-login.png">
</h4>

<a id="tecnologias-utilizadas"></a>
## :rocket: Tecnologias Utilizadas

O projeto foi desenvolvido utilizando as seguintes tecnologias

- [React]
- [Axios]
- [Styled Components]
- [yup]

<a id="como-usar"></a>
## :fire: Como usar

- Clone esse repositório: `git clone https://pablotmagalhaes@bitbucket.org/pablotmagalhaes/empresas-web.git`
- Instale as dependências: `yarn` 
- Start a aplicação: `yarn start`

## :Dados de acesso:

<b>Usuário de Teste:</b> testeapple@ioasys.com.br
<b>Senha de Teste</b> : 12341234

<b>Obs</b> : Pode demorar um pouco para autenticar por conta de uma intermitência na API.

## :memo: License

Esse projeto está sob a licença MIT. Veja o arquivo [LICENSE](LICENSE) para mais detalhes.

---

<h4 align="center">
    Feito com 💜 by <a href="https://www.linkedin.com/in/thiagomagalhaesme/" target="_blank">Pablo Thiago Magalhães</a>
</h4>

