import { createGlobalStyle } from 'styled-components';


export default createGlobalStyle`

@import url(‘https://fonts.googleapis.com/css?family=Roboto:400,900|Montserrat');


* {
  margin: 0;
  padding: 0;
  outline: 0;
  box-sizing: border-box;
}
*:focus {
  outline: 0;
}
html, body, #root {
  width: 100%;
  height: 100%;
}
body {
  -webkit-font-smoothing: antialiased;
  background: #ebe9d7;
}
button, input, body{
  font: 14px 'Roboto', sans-serif;
}
a{
  text-decoration: none;
}
ul {
  list-style: none;
}

button{
  cursor: pointer;
}

`;
