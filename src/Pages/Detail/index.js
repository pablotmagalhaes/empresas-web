import React from 'react';
import { MdArrowBack } from 'react-icons/md';
import { Container, Content, Header } from './styles';
import { useLocation, useHistory } from 'react-router-dom';

const Detail = () => {
  const { enterprise } = useLocation();
  const history = useHistory();
  return (
    <>
      <Header>
        <button type='button' onClick={history.goBack}>
          <MdArrowBack size={30} />
        </button>
        <h1>{enterprise.enterprise_name}</h1>
      </Header>
      <Container>
        <Content>
          <img src='https://via.placeholder.com/388x147.jpg' alt={enterprise.enterprise_name} />
          <p>{enterprise.description}</p>
        </Content>
      </Container>
    </>
  );
};
export default Detail;
