import React, { useState } from 'react';
import { useAuth } from '../../Context/auth';
import LoadingOverlay from 'react-loading-overlay';
import { BsEyeFill, BsEyeSlashFill } from 'react-icons/bs';


import { Container, Content, FieldInput, Button, Form } from './styles.js';
import { FiLock, FiMail } from 'react-icons/fi';
import { MdError } from 'react-icons/md';
import logoHome from '../../assets/images/logo-home.png';

const SignIn = () => {
  const { signIn, isLoading, hasLoginError, clearLoginError } = useAuth();
  const [email, setEmail] = useState('testeapple@ioasys.com.br');
  const [password, setPassword] = useState('12341234');
  const [typeInput, setTypeInput] = useState('password');

  const handleSubmit = async (e) => {
    e.preventDefault();
    await signIn(email, password);
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
    clearLoginError();
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
    clearLoginError();
  };

  const handleTypeInput = () => {
    typeInput === 'password' ? setTypeInput('text') : setTypeInput('password');
  };

  return (
    <>
   
      <LoadingOverlay active={isLoading} spinner text='Loading...'>
      
      <Container>
        <Content>
          <img src={logoHome} alt='logo-Ioasys' />
          <h1>Bem-vindo ao empresas</h1>
          <p>
            Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
          </p>
          <Form onSubmit={(e) => handleSubmit(e)}>
            <FieldInput>
              <FiMail size={16} />
              <input
                type='text'
                placeholder='E-mail'
                value={email}
                onChange={(e) => handleEmailChange(e)}
              />
              {hasLoginError && <MdError size={16} />}
            </FieldInput>
            <FieldInput>
              <FiLock size={16} />
              <input
                type={typeInput}
                placeholder='Senha'
                value={password}
                onChange={(e) => handlePasswordChange(e)}
              />
              {hasLoginError ? (
                <MdError size={16} />
              ) : (
                <button type='button' className='button-eyes' onClick={handleTypeInput}>
                  {typeInput === 'password' ? <BsEyeFill /> : <BsEyeSlashFill />}
                </button>
              )}
            </FieldInput>
            {hasLoginError && (
              <p>Credenciais informadas são inválidas, tente novamente.</p>
            )}
            <Button primary={hasLoginError} type='submit'>
              ENTRAR
            </Button>
          </Form>
        </Content>
      </Container>
    </LoadingOverlay>  
    </>
  );
};

export default SignIn;
