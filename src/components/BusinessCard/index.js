import React from 'react';
import { Container } from './styles';
import { Link } from 'react-router-dom';

const BusinessCard = ({ enterprise }) => {
  return (
    <Link to={{ pathname: `/detail/${enterprise.id}`, enterprise }}>
      <Container>
        <div className='image-container'>
          <img
            src='https://via.placeholder.com/293x160.jpg'
            alt={enterprise.enterprise_name}
          />
        </div>
        <div className='description-container'>
          <h1>{enterprise.enterprise_name}</h1>
          <p>{enterprise.enterprise_type.enterprise_type_name}</p>
          <strong>{enterprise.country}</strong>
        </div>
      </Container>
    </Link>
  );
};

export default BusinessCard;
