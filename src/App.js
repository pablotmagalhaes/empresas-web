import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './routes';
import AppProvider from './Context';
import MainStyle from './styles/main';



function App() {
  return (
    <Router>
      <AppProvider>
        <Routes />
      </AppProvider>
      <MainStyle />
    </Router>
  );
}

export default App;
