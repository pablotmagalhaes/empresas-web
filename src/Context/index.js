import React from 'react';
import { EnterpriseProvider } from './business';
import { AuthProvider } from './auth';

const AppProvider = ({ children }) => (
  <AuthProvider>
    <EnterpriseProvider>{children}</EnterpriseProvider>
  </AuthProvider>
);

export default AppProvider;
