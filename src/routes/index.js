import React from 'react';
import { Switch } from 'react-router-dom';

import Route from './Route';

import SignIn from '../Pages/SignIn';
import Home from '../Pages/Home';
import Detail from '../Pages/Detail';

const Routes = () => (
  <Switch>
    <Route path='/' exact component={SignIn} />
    <Route path='/home' component={Home} isPrivate />
    <Route path='/detail/:id' component={Detail} isPrivate />
  </Switch>
);

export default Routes;